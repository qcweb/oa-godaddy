# README #

### Magento OA Shop ###

* Quick summary
* Version 1.0

### Quick Summary ###

* OA E-Commerce store based on Magento. Custom responsive HTML5 solution for OA Shop. 
* Package: oa_commerce
* Theme: oa
* Modules: Daily Specials

### Contact ###

* rafael.hernand3z@gmail.om
* Thanks to Mike, Nabil, Freddy & Norman for all your hard work.