<?php
require_once(Mage::getModuleDir('controllers','Mage_Checkout').DS.'CartController.php');
//require_once "Mage/Checkout/controllers/CartController.php";
class Kairoz_Checkout_CartController extends Mage_Checkout_CartController
{
    /*# Rewrite of indexAction
    public function indexAction() {
        die('your method has been reffwrited !!');
    }*/

    public function addAction()
    {
    	if (!$this->_validateFormKey()) {
        $this->_goBack();
        return;
      }
      $cart   = $this->_getCart();
      $params = $this->getRequest()->getParams();
      // Begin Ajax code
      if($params['isAjax'] == 1) {
      	$response = array();
      	try {
          if(isset($params['qty'])) {
            $filter = new Zend_Filter_LocalizedToNormalized( array('locale' => Mage::app()->getLocale()->getLocaleCode()));
            $params['qty'] = $filter->filter($params['qty']);
          }

          $product = $this->_initProduct();
          $related = $this->getRequest()->getParam('related_product');

          /**
          * Check product availability
          */
          if(!$product) {
            $response['status'] = 'ERROR';
            $response['message'] = $this->__('Unable to find product id');
          }

          $cart->addProduct($product, $params);
          if(!empty($related)) {
            $cart->addProductsByIds(explode(',',$related));
          }

          $cart->save();

          $this->_getSession()->setCartWasUpdated(true);

          /**
          * @todo remove wishlist observer processAddToCart
          */
          Mage::dispatchEvent('checkout_cart_add_product_complete',
            array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
          );

          if (!$this->_getSession()->getNoCartRedirect(true)) {
            if (!$cart->getQuote()->getHasError()) {
              $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
              //$this->_getSession()->addSuccess($message);
              $response['status'] = 'SUCCESS';
              $response['message'] = $message;
              $this->loadLayout();
              $toplink = $this->getLayout()->getBlock('top.links')->toHtml();
              $sidebar = $this->getLayout()->getBlock('cart_sidebar')->toHtml();
              $response['toplink'] = $toplink;
              $response['sidebar'] = $sidebar;
              $response['cartCount'] = Mage::helper('checkout/cart')->getSummaryCount();
            }
            //$this->_goBack();
          }

      	} catch (Mage_Core_Exception $e) {
          $msg = '';
          if($this->_getSession()->getUseNotice(true)) {
            $msg = $e->getMessage();
          }else {
            $messages = array_unique(explode("\n", $e->getMessage()));
            foreach($messages as $message) {
              $msg .= $message."<br/>";
            }
          }
          $response['status'] = 'ERROR';
          $response['message'] = $msg;
        } catch (Exception $e) {
          $response['status'] = 'ERROR';
          $response['message'] = $this->__('Cannot add item to the shopping cart.');
          Mage::logException($e);
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($response));
        return;
      }else {
      	//Process Normally
        return parent::addAction();
      }
      // End Ajax code
        /* //Old Controller Function
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $product = $this->_initProduct();
            $related = $this->getRequest()->getParam('related_product');

            /**
             * Check product availability
             *
            if (!$product) {
                $this->_goBack();
                return;
            }

            $cart->addProduct($product, $params);
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            /**
             * @todo remove wishlist observer processAddToCart
             *
            Mage::dispatchEvent('checkout_cart_add_product_complete',
                array('product' => $product, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );

            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__('%s was added to your shopping cart.', Mage::helper('core')->escapeHtml($product->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice(Mage::helper('core')->escapeHtml($e->getMessage()));
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError(Mage::helper('core')->escapeHtml($message));
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot add the item to shopping cart.'));
            Mage::logException($e);
            $this->_goBack();
        }
        */
    }

    /**
     * Update product configuration for a cart item
     */
    public function updateItemOptionsAction()
    {
        $cart   = $this->_getCart();
        $id = (int) $this->getRequest()->getParam('id');
        $params = $this->getRequest()->getParams();

        if (!isset($params['options'])) {
            $params['options'] = array();
        }
        try {
            if (isset($params['qty'])) {
                $filter = new Zend_Filter_LocalizedToNormalized(
                    array('locale' => Mage::app()->getLocale()->getLocaleCode())
                );
                $params['qty'] = $filter->filter($params['qty']);
            }

            $quoteItem = $cart->getQuote()->getItemById($id);
            if (!$quoteItem) {
                Mage::throwException($this->__('Quote item is not found.'));
            }

            $item = $cart->updateItem($id, new Varien_Object($params));
            if (is_string($item)) {
                Mage::throwException($item);
            }
            if ($item->getHasError()) {
                Mage::throwException($item->getMessage());
            }

            $related = $this->getRequest()->getParam('related_product');
            if (!empty($related)) {
                $cart->addProductsByIds(explode(',', $related));
            }

            $cart->save();

            $this->_getSession()->setCartWasUpdated(true);

            Mage::dispatchEvent('checkout_cart_update_item_complete',
                array('item' => $item, 'request' => $this->getRequest(), 'response' => $this->getResponse())
            );
            if (!$this->_getSession()->getNoCartRedirect(true)) {
                if (!$cart->getQuote()->getHasError()) {
                    $message = $this->__('%s was updated in your shopping cart.', Mage::helper('core')->escapeHtml($item->getProduct()->getName()));
                    $this->_getSession()->addSuccess($message);
                }
                $this->_goBack();
            }
        } catch (Mage_Core_Exception $e) {
            if ($this->_getSession()->getUseNotice(true)) {
                $this->_getSession()->addNotice($e->getMessage());
            } else {
                $messages = array_unique(explode("\n", $e->getMessage()));
                foreach ($messages as $message) {
                    $this->_getSession()->addError($message);
                }
            }

            $url = $this->_getSession()->getRedirectUrl(true);
            if ($url) {
                $this->getResponse()->setRedirect($url);
            } else {
                $this->_redirectReferer(Mage::helper('checkout/cart')->getCartUrl());
            }
        } catch (Exception $e) {
            $this->_getSession()->addException($e, $this->__('Cannot update the item.'));
            Mage::logException($e);
            $this->_goBack();
        }
        $this->_redirect('*/*');
    }
}

?>