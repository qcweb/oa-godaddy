<?php

class Kairoz_DisplayPercentageSavings_Helper_Data extends Mage_Core_Helper_Abstract{

	public function getPercentageSavings($originalPrice, $newPrice, $finalPrice = 0){
		if(!isset($originalPrice) || !isset($newPrice)){
			return 0;
		}
    if( $newPrice > $originalPrice ) return 0;
    if( $finalPrice == $originalPrice ) return 0;
		return round((($originalPrice - $newPrice)/$originalPrice) * 100);
	}

}

?>