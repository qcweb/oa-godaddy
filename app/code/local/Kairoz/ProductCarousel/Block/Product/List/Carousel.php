<?php
/**
*		Product Carousel Widget
*		
*		@author Michael Lee
*/

class Kairoz_ProductCarousel_Block_Product_List_Carousel
	extends Mage_Catalog_Block_Product_Abstract
	implements Mage_Widget_Block_Interface 
{

	public function getTitle()
	{
		$selectedCarousel = $this->getData('product_category');
		$titleArray = explode('_', $selectedCarousel);
		if(count($titleArray) < 2){
			return 'Product Slider';
		}
		$title = ucfirst($titleArray[0]).' '.ucfirst($titleArray[1]);

		return $title;
	}

	public function getProducts()
	{
		$slider_type = $this->getData('product_category');

		if($slider_type === 'best_sellers'){
			return $this->_getBestSellingProducts();
		}elseif($slider_type === 'new_products'){
			return $this->_getNewestProducts();
		}
		
	}

	private function _getBestSellingProducts()
	{
		$visibility = array(
			Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
			Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG);

		$_productCollection = Mage::getResourceModel('reports/product_collection')
			->addAttributeToSelect('*')
			->addOrderedQty()
			->addAttributeToFilter('visibility', $visibility)
			->setOrder('ordered_qty', 'desc');

		$productCount = $this->getData('display_count');

		$starting_from = 0;

		$_productCollection->getSelect()->limit($productCount, $starting_from);

		return $_productCollection;
	}

	private function _getNewestProducts()
	{
		$visibility = array(
			Mage_Catalog_Model_Product_Visibility::VISIBILITY_BOTH,
			Mage_Catalog_Model_Product_Visibility::VISIBILITY_IN_CATALOG);

		$collection = Mage::getResourceModel('catalog/product_collection')
			->addAttributeToSelect('*')
			->addAttributeToFilter('visibility', $visibility)
			->setOrder('created_at', 'desc');

		$productCount = $this->getData('display_count');

		$starting_from = 0;

		$collection->getSelect()->limit($productCount, $starting_from);

		/*
		$collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToSort('created_at', 'desc');
		*/

    return $collection;
	}

	/*
	protected function _getRecentlyAddedProductsCollection()
    {
        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection /
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToSort('created_at', 'desc')
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1)
        ;
        return $collection;
    }
  */
}