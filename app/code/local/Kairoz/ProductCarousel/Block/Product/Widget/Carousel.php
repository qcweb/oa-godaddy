<?php
/**
*		Product Carousel Widget
*		
*		@author Michael Lee
*/

class Kairoz_ProductCarousel_Block_Product_Widget_Carousel
	//extends Mage_Core_Block_Template //Used for template options
	extends Mage_Catalog_Block_Product_Abstract //Needed to get information
	implements Mage_Widget_Block_Interface 
{

	protected function _toHtml()
	{
		return parent::_toHtml();
	}

	public function getTitle()
	{
		$selectedCarousel = $this->getData('product_category');
		$title = ($selectedCarousel === 'new_products') ? 'New Items' : 'Best Sellers';
    if($selectedCarousel === 'new_products') {
      $title = 'New Items';
    }elseif ($selectedCarousel === 'best_sellers') {
      $title = 'Best Sellers';
    }elseif ($selectedCarousel === 'recommended') {
      $title = 'Recommendations';
    }

		return $title;
	}

	public function getCarouselType()
	{
		$selectedCarousel = $this->getData('product_category');
		if($selectedCarousel === 'new_products'){
			$carouselType = 'new_';
		}elseif($selectedCarousel === 'best_sellers'){
			$carouselType = 'best_';
		}elseif($selectedCarousel === 'recommended'){
      $carouselType = 'related_';
    }
		return $carouselType;
	}

	public function getProducts()
	{
		$slider_type = $this->getData('product_category');

		if($slider_type === 'best_sellers'){
			return $this->_getBestSellingProducts();
		}elseif($slider_type === 'new_products'){
			return $this->_getNewestProducts();
		}elseif ($slider_type === 'recommended') {
      return $this->_getRecommendedProducts();
    }
		
	}

	protected function getProductsCount()
	{
		$productCount = $this->getData('display_count');
		return $productCount;
	}

	private function _getNewestProducts()
	{
		$collection = Mage::getResourceModel('catalog/product_collection');
		$collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
    //Filter for status enabled
    $collection->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);

		$productCount = $this->getData('display_count');

		$starting_from = 0;

    $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status',1) //For Status Enabled
            ->addAttributeToSort('created_at', 'desc')
            ->setPageSize($productCount)
            ->setCurPage(1);

    //Filter for only products in stock
    Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);

    return $collection;
	}

	private function _getBestSellingProducts()
	{
    $collection = Mage::getResourceModel('reports/product_collection');
    $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
    $productCount = $this->getData('display_count');

    $starting_from = 0;

    $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addOrderedQty()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status',1) //For Status Enabled
            ->addAttributeToSort('ordered_qty', 'desc')
            ->setPageSize($productCount)
            ->setCurPage(1);

    //Filter for only products in stock
    Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);

    return $collection;
	}

  private function _getRecommendedProducts()
  {
    $cur_product = Mage::registry('current_product');

    $collection = $cur_product->getRelatedProductCollection()
                ->addAttributeToSelect('required_options')
                ->setPositionOrder()
                ->addStoreFilter()
    ;

    if (Mage::helper('catalog')->isModuleEnabled('Mage_Checkout')) {
            Mage::getResourceSingleton('checkout/cart')->addExcludeProductFilter($collection,
                Mage::getSingleton('checkout/session')->getQuoteId()
            );
            $this->_addProductAttributesAndPrices($collection);
    }

    Mage::getSingleton('catalog/product_visibility')->addVisibleInCatalogFilterToCollection($collection);

    return $collection;
  }

	/*
	protected function _getRecentlyAddedProductsCollection()
    {
        /** @var $collection Mage_Catalog_Model_Resource_Product_Collection /
        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());

        $collection = $this->_addProductAttributesAndPrices($collection)
            ->addStoreFilter()
            ->addAttributeToSort('created_at', 'desc')
            ->setPageSize($this->getProductsCount())
            ->setCurPage(1)
        ;
        return $collection;
    }
  */
}