<?php
class Kairoz_DailySpecials_Adminhtml_DailyspecialsController extends Mage_Adminhtml_Controller_Action
{
    /**
     * Redirect to Daily Specials Category
     *
     */
    public function indexAction()
    {      
        $read   = Mage::getSingleton("core/resource")->getConnection("core_read");
        $query  = "SELECT dailyspecials_category_id FROM mage_kairoz_dailyspecials WHERE dailyspecials_id = 1";
        $result = $read->fetchRow($query);

        $categoryId = 0;
        if (isset($result))
            $categoryId = $result['dailyspecials_category_id'];

        Mage::getSingleton('core/session')->setIsDailySpecials($categoryId);

    	$url = Mage::helper("adminhtml")->getUrl("/catalog_category/");
        $this->getResponse()->setRedirect($url);
    }
}