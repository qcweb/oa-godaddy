<?php
	class Kairoz_DailySpecials_Model_Observer {
		public function setIsDailySpecialCategory(Varien_Event_Observer $observer) {
			$isDailySpecials = Mage::getSingleton('core/session')->getIsDailySpecials();

			if ($isDailySpecials) {
				Mage::app()->getRequest()->setParam('id', $isDailySpecials);
				Mage::app()->getRequest()->setParam('active_tab_id', 'category_info_tabs_products');
				Mage::getSingleton('admin/session')->setLastEditedCategory($isDailySpecials);
			}
		}

		public function unsetIsDailySpecialCategory(Varien_Event_Observer $observer) {
			$isDailySpecials = Mage::getSingleton('core/session')->getIsDailySpecials();

			if ($isDailySpecials) {
				Mage::getSingleton('core/session')->setIsDailySpecials(false);
			}
		}
	}