<?php
	$installer = $this;
	$installer->startSetup();

	$installer->run("
		DROP TABLE IF EXISTS {$this->getTable('kairoz_dailyspecials')};
		CREATE TABLE {$this->getTable('kairoz_dailyspecials')} (
			`dailyspecials_id` int(10) unsigned NOT NULL auto_increment,
			`dailyspecials_store_id` int(10) unsigned NOT NULL,
			`dailyspecials_category_id` int(10) unsigned NOT NULL,
			PRIMARY KEY  (`dailyspecials_id`)
		) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='ACL Asserts';
	");

	Mage::init();//Needed to access store variables

	//Use the store id to get the current root category 
	$storeId        = Mage::app()->getStore()->getId();
	$rootCategoryId = Mage::app()->getStore($storeId)->getRootCategoryId();

	$category = Mage::getModel('catalog/category');//create a new category
	$rootCategory = Mage::getModel('catalog/category')->load($rootCategoryId);//get current root category

	//configure new category
	$category->setName('Daily Specials')
	->setIsActive(0)//make categor inaccessible from the frontend
	->setDisplayMode('PRODUCTS')
	->setIsAnchor(1)
	->setCustomDesignApply(1)
	->setAttributeSetId($category->getDefaultAttributeSetId());

	//set the parent category of new Daily Specials category to the store's root category
	$category->setPath($rootCategory->getPath());
	$category->save();

	$newCategoryId = $category->getId();

	$installer->run("
		INSERT INTO {$this->getTable('kairoz_dailyspecials')}
		(`dailyspecials_store_id`,`dailyspecials_category_id`)
		VALUES ( ".$storeId.",".$newCategoryId." )
	");

	$installer->endSetup();
?>