<?php
class Kairoz_MasonryWidget_Block_Widget
    extends Mage_Core_Block_Template
    implements Mage_Widget_Block_Interface
{
    /**
     * A model to serialize attributes
     * @var Varien_Object
     */
    protected $_serializer = null;

    /**
     * Initialization
     */
    protected function _construct()
    {
        $this->_serializer = new Varien_Object();
        parent::_construct();
    }

    /**
     * Produces masonry widget html
     *
     * @return string
     */
    protected function _toHtml()
    {
        return parent::_toHtml();
    }

    public function getProducts() {
        $todayDate = date();
        $products = Mage::getModel('catalog/category')->load($this->getCategory())
        ->getProductCollection()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('status', 1)
        ->addAttributeToFilter('visibility', 4)
        ->setPageSize(8);

        return $products;
    }

    /**
     * Retrieve the id of the category that was selected
     *
     * @return int
     */
    public function getCategory()
    {
        if (!$this->hasData('category')) {
            return parent::getCategory();
        }
        return $this->getData('category');
    }

     /**
     * Retrieve the title that should be displayed above featured products
     * @return int
     */
    public function getTitle()
    {
        if (!$this->hasData('title')) {
            return parent::getTitle();
        }
        return $this->getData('title');
    }

}