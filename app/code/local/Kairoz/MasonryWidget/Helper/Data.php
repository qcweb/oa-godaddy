<?php
    /**
 *Masonry Widget Helper
 */
class Kairoz_MasonryWidget_Helper_Data extends Mage_Core_Helper_Abstract
{
    /**
     * Determine if product is on speial
     *
     * @return boolean
    */
    public function isSpecial($product) {
        // Get the Special Price
        $specialprice = $product->getSpecialPrice();
        // Get the Special Price FROM date
        $specialPriceFromDate = new DateTime($product->getSpecialFromDate());
        // Get the Special Price TO date
        $specialPriceToDate = new DateTime($product->getSpecialToDate());
        // Get Current date
        $today =  new DateTime("now");
        $today->setTime(0,0);

        if ($specialprice) {
            if( $today >= $specialPriceFromDate && $today <= $specialPriceToDate ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Determine if product is new
     * @return boolean
    */
    public function isNew($product) {
        //new products
        $recentlyAdded = false;
        $newProducts = $this->_getNewestProducts();
        $pID = $product->getId();

        foreach ($newProducts as $_product):
            $newProductId = $_product->getId();

            if ($newProductId == $pID) {
                $recentlyAdded = true;
                break;
            }

        endforeach;

        //Get the New FROM date
        $newFromDate =  new DateTime($product->getNewsFromDate());
        // Get the New TO date
        $newToDate = new DateTime($product->getNewsToDate());
        // Get Current date
        $today =  new DateTime("now");
        $today->setTime(0,0);

        if ($newFromDate) {
            if ( $today >=  $newFromDate && $today <= $newToDate || $recentlyAdded ) {
                return true;
            }
        }

        return false;
    }

    private function _getNewestProducts()
    {
        $productCount = 8;

        $collection = Mage::getResourceModel('catalog/product_collection');
        $collection->setVisibility(Mage::getSingleton('catalog/product_visibility')->getVisibleInCatalogIds());
        //Filter for status enabled
        $collection->addFieldToFilter('status', Mage_Catalog_Model_Product_Status::STATUS_ENABLED);

        $collection->addStoreFilter()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status',1) //For Status Enabled
            ->addAttributeToSort('created_at', 'desc')
            ->setPageSize($productCount);

        //Filter for only products in stock
        Mage::getSingleton('cataloginventory/stock')->addInStockFilterToCollection($collection);

        return $collection;
    }
}