<?php
class Kairoz_MasonryWidget_Model_Categories {

	/**
     * Provide available options as a value/label array
     *
     * @return array
     */
    public function toOptionArray() {
		$category = Mage::getModel('catalog/category');
		$tree = $category->getTreeModel();
		$tree->load();
		$ids = $tree->getCollection()->getAllIds();

		$optionArray = array();

		if ($ids) {
		    foreach ($ids as $id) {
		        $cat = Mage::getModel('catalog/category');
		        $cat->load($id);

		        $entity_id = $cat->getId();
		        $name = $cat->getName();
		        $url_key = $cat->getUrlKey();
		        $url_path = $cat->getUrlPath();

		        $optionArray[] = array('value' => $entity_id, 'label' => $name);
		    }
		}

		return $optionArray;
	}
}