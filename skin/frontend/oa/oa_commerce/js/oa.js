var $j = jQuery.noConflict();
$j(function () {
	/*
	*   oa_menu variable will hold the functions that will open and close the menu
	*   Two global listeners is the click on the trigger and a click on the html body
	*   Variable isMenuOpened is a boolean value to determine the state of the menu
	*
	*   Parameters:
	*   -triggerId: id of the element that will activate the mobile menu
	*   -navId: id of the element that holds the menu
	*/
	var oa_menu = {
		isMenuOpened: false,
		toggleOn: false,

		init : function (triggerId, navId) {

			var trigger = $j(triggerId);
			var nav = $j(navId);

			// make it work on touch screen device and mouse click
			trigger.on('touchstart click', function (e) {
				e.stopPropagation();
				e.preventDefault();
				oa_menu.toggleMenu(trigger,nav);
			});

			//If loaded on desktop enable body click listener
			if (!oa_menu.isMobile()) {
				oa_menu.bodyClick(trigger,nav);
			}
		},


		/* Function listens for when the body is clicked on
		* If Menu is Open it will hide the menu
		*
		*/
		bodyClick: function (trigger, nav) {
			$j('html').bind('click',function () {
				if (oa_menu.isMenuOpened){
					oa_menu.hideMenu(trigger,nav);
				}
			});
		},

		/*
		*   toggleSubOn
		*   1. If toggleOn (toggle for subcategories) is turned off turn it on
		*   2. Add event .on(click) and add the slide toggle
		*   3. Set toggleOn to true
		*/
		toggleSubOn: function() {
			if (!oa_menu.toggleOn) {

				var level_top_link = $j('li.parent > a');

				level_top_link.on('touchstart click', function () {
					if( $j(this).next().hasClass('sub-menu-wrapper') ) {
						$j(this).next().slideToggle();
					}
					//same as prevent default()
					return false;
				});
				oa_menu.toggleOn = true;

			}
		},

		/*
		*  toggleSubOff
		*   1.Checks to see if toggleOn (If on.Click is on)
		*   2.If true we want to turn it off, set toggleOn to false
		*   3.Removes the styles that was added by slideToggle
		*/
		/*toggleSubOff: function() {

			if( $j('.sub-menu-wrapper').hasClass('open') ) {
				$j(this).removeClass('open');
			}

			oa_menu.toggleOn = false;

		}, */

		/*  Toggle Menu decides if we need to
		*   1.Open the menu
		*   2.Close the menu
		*/
		toggleMenu: function (trigger, nav) {
			if (oa_menu.isMenuOpened) {
				oa_menu.hideMenu(trigger, nav);
			}
			else {
				oa_menu.showMenu(trigger, nav);
			}
			if(oa_search.isSearchOpened){
				oa_search.hideSearch($j("#responsive-search-trigger"),$j("#search-container"));
			}
			if(oa_account.isAccountOpened) {
				oa_account.hideAccount($j("#responsive-account-trigger"), $j(".mobile-account"));
			}
		},

		/* Show Menu function adds the necessary classes
		*  Sets isMenuOpened to true
		*
		*/
		showMenu: function (trigger, nav) {
			trigger.addClass('active');
			nav.addClass('show-menu');
			$j('#overlay').addClass('mobile-menu-active');
			oa_menu.isMenuOpened = true;
			oa_menu.toggleSubOn();
		},

		/* Hide Menu function removes the added classes
		*  Sets isMenuOpened to false
		*/
		hideMenu: function (trigger,nav) {
			trigger.removeClass('active');
			nav.removeClass('show-menu');
			$j('#overlay').removeClass('mobile-menu-active');
			oa_menu.isMenuOpened = false;

			if( $j('.sub-menu-wrapper').hasClass('open') ) {
				$j('.sub-menu-wrapper').removeClass('open');
			}

		},

		/*
		*   Checks to see if the site is being viewed on a mobile site
		*/
		isMobile: function() {
			var check = false;
			if ( !$j('#mobile-control').length ) {
				check = true;
			}
			return check;
		}
	};

	/*
	*		oa_mega variable will hold the functions that will open and close the mega menu
	*		Two global listeners is the click on the trigger and a click on the html body
	*		Variable isMenuOpened is a boolean value to determine the state of the menu
	*
	*		Parameters:
	*
	*
	*/
	var oa_mega = {

		isMegaMenuOpened: false,
		init : function (navId) {
			var nav = $j(navId);

			//top menu
			var topLevelMenuItems = nav.find('.top-level >li.parent');
			topLevelMenuItems.hover(
				function (e) {
					e.stopPropagation();
					e.preventDefault();
					$j(this).addClass('show-mega');
				},
				function (e) {
					e.preventDefault();
					$j(this).removeClass('show-mega');
				}
			);
		}
	};




	/*End of oa_search variable*//*
	*
	*	oa_search variable will hold the functions that will open and close the search component
	* This js is only applied to the following views: Mobile | Tablet
	*
	*/
	var oa_search = {
		isSearchOpened: false,
		init: function (responsiveSearchTrigger,searchComponentId) {

			var trigger = $j(responsiveSearchTrigger);
			var search = $j(searchComponentId);

			//Have transition work on touch screen device and mouse click
			trigger.on('touchstart click', function (e) {
				e.stopPropagation();
				e.preventDefault();
				oa_search.toggleSearch(trigger,search);
			});

			//If loaded on desktop enable body click listener
			if(!oa_search.isMobile()){
				oa_search.bodyClick(trigger,search);
			}
		},
		/* End of init function */


		/* Function listens for when the body is clicked on
		* If Search is open it must close or hide the search
		*
		*
		*/
		bodyClick: function(trigger,search) {
			$j('.wrapper').bind('click' ,function () {
				if (oa_search.isSearchOpened){
					oa_search.hideSearch(trigger,search);
				}
			});
		},
		/* End of body Click */

		/* Toggle Search decides if we need to
		*  1.Open the Search
		*  2.Close the Search
		*/
		toggleSearch: function(trigger, search) {
			if (oa_search.isSearchOpened) {
				oa_search.hideSearch(trigger,search);
			}
			else{
				oa_search.showSearch(trigger,search);
			}

			if(oa_account.isAccountOpened) {
				oa_account.hideAccount($j("#responsive-account-trigger"), $j(".mobile-account"));
			}
			if(oa_menu.isMenuOpened) {
				oa_menu.toggleMenu($j("#mobile-control"), $j("#magento-nav"));
				oa_search.toggleSearch(trigger, search);
			}
		},
		/*End of Toggle Search*/

		showSearch: function (trigger,search) {
			trigger.addClass('active-search');
			search.addClass('show-search');
			oa_search.isSearchOpened = true;
		},

		hideSearch: function (trigger,search) {
			trigger.removeClass('active-search');
			search.removeClass('show-search');
			oa_search.isSearchOpened = false;
		},

		isMobile: function() {
			var check = false;
			if ( !$j('#mobile-control').length ) {
				check = true;
			}
			return check;
		}

	};

	/*End of oa_account variable*//*
	*
	*	oa_account variable will hold the functions that will open and close the account component
	* This js is only applied to the following views: Mobile | Tablet
	*
	*/
	var oa_account = {
		isAccountOpened: false,
		init: function (responsiveAccountTrigger,accountComponentId) {

			var trigger = $j(responsiveAccountTrigger);
			var account = $j(accountComponentId);

			//Have transition work on touch screen device and mouse click
			trigger.on('touchstart click', function (e) {
				e.stopPropagation();
				e.preventDefault();
				oa_account.toggleAccount(trigger,account);
			});

			//If loaded on desktop enable body click listener
			if(!oa_account.isMobile()){
				oa_account.bodyClick(trigger,account);
			}
		},
		/* End of init function */


		/* Function listens for when the body is clicked on
		* If Account is open it must close or hide the account
		*
		*
		*/
		bodyClick: function(trigger,account) {
			$j('.wrapper').bind('click' ,function () {
				if (oa_account.isAccountOpened){
					oa_account.hideAccount(trigger,account);
				}
			});
		},
		/* End of body Click */

		/* Toggle Account decides if we need to
		*  1.Open the Account
		*  2.Close the Account
		*/
		toggleAccount: function(trigger, account) {
			if (oa_account.isAccountOpened) {
				oa_account.hideAccount(trigger,account);
			}
			else{
				oa_account.showAccount(trigger,account);
			}
			//search = $j("#responsive-search-trigger");
			if(oa_search.isSearchOpened){
				oa_search.hideSearch($j("#responsive-search-trigger"),$j("#search-container"));
			}
			if(oa_menu.isMenuOpened) {
				oa_menu.toggleMenu($j("#mobile-control"), $j("#magento-nav"));
				oa_account.toggleAccount(trigger, account);
			}
		},
		/*End of Toggle Account*/

		showAccount: function (trigger,account) {
			trigger.addClass('active-account');
			account.addClass('show-account');
			oa_account.isAccountOpened = true;
		},

		hideAccount: function (trigger,account) {
			trigger.removeClass('active-account');
			account.removeClass('show-account');
			oa_account.isAccountOpened = false;
		},

		isMobile: function() {
			var check = false;
			if ( !$j('#mobile-control').length ) {
				check = true;
			}
			return check;
		}

	};

	(function(jQuery) {
		function isMobile() {
			//console.log(jQuery(document).width());
			if (jQuery(document).width() < 768) return true;
			return false;
		}
		jQuery('p.rating-links > a').click(function() {
			if(!isMobile()) {
				jQuery('div.tabs-landscape').find('h4.active').removeClass('active');
				jQuery('#customer-reviews').parent('h4').addClass('active');
				var toHide = jQuery('div#tab-accordion').find('div.info-tab > div.active').removeClass('active').hide();
				jQuery('div#tab-accordion > div#customer-reviews-mobile > div.customer-reviews > div#tab-reviews').addClass('active').show();
				//jQuery('div#tab-accordion').find('div.customer-reviews-mobile')
				jQuery('#customer-reviews').trigger('click');
			}else {
				jQuery('#customer-reviews-mobile > div.customer-reviews > h4.collapse-header').trigger('click');
				window.location.hash = '#customer-reviews-mobile';
			}
		});
	})(jQuery)

	/* Initialize Mobile Navigation */
	oa_mega.init("#magento-nav");

	/* Initialize Mobile Navigation */
	oa_menu.init("#mobile-control", "#magento-nav");

	/* Initialize Search for mobile */
	oa_search.init("#responsive-search-trigger","#search-container");

	/* Initialize Account for mobile */
	oa_account.init("#responsive-account-trigger",".mobile-account");

	/* Initialize Back To Top Utility */
		var back_top = {

		init: function(anchorId){
			var backToTop = $j(anchorId);
			backToTop.click(function(){
				$j('html, body').animate({scrollTop:0}, 'slow');
			});
		}
	}

	back_top.init("#back-top-trigger");

	/*
		Run placeholder.js
		This include IE8 and IE9 specific fixes for the placeholder and various related CSS
	*/
	if (jQuery.browser.msie && jQuery.browser.version < 11) {
	  /* Target IE9 and older */

  	if (jQuery.browser.version < 10) {

	    if (jQuery.browser.version != 9) {
	      // Only apply to account/login/dashboard pages
	      $j("input[type=text], input[type=password], textarea, .placeholder").css("padding-top", "15px");

	      // Fix Checkboxes not being clickable:
	      $j(".custom-checkbox").css("display", "inline");
	      $j(".custom-checkbox-label").addClass("ie-label");
	      $j("label").removeClass("custom-checkbox-label");
	      $j(".ie-label span").css("display", "inline-block");
	      $j(".ie-label span").css("margin", "0");
	    }

	    // Fix border and placeholder color for input fields on IE8 & IE9
	    $j('input[type=text], textarea, input[type=password]').css("border-bottom", "1px solid #cccccc");
	    $j('input[type=text], textarea, input[type=password]').placeholder();
	    $j(".placeholder").css("color", "#8ea395");
	  }
    // Fix issue with addthis position on IE10 and below
    $j(".share-product").css("margin-top", "-52px");
	}
});